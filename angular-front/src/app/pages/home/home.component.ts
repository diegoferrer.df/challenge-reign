
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ArticlesService } from 'src/app/core/services/articles.service';
import { Article } from 'src/app/shared/interfaces/article.interface';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  deleteArticleId:number = 0
  articles$:Observable<Article[]> | undefined;

  constructor(private articleService:ArticlesService,private route: Router) { }

  ngOnInit(): void {
    this.getAllArticles()
  }

  getAllArticles(){
    this.articles$ = this.articleService.getArticles()
  }

  deleteArticle(id:number){
    this.articleService.deleteArticle(id).subscribe(response => {
      this.deleteArticleId = id
    },error => this.route.navigate(['error']))
  }


  logout(){
    sessionStorage.clear();
    this.route.navigate(['/login']);
  }

}
