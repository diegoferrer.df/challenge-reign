import { HttpBackend, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Article } from 'src/app/shared/interfaces/article.interface';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ArticlesService {

  private BASE_URL: string;
  private httpClient: HttpClient;

  constructor(private http:HttpClient,private httpBackend: HttpBackend) {
    this.BASE_URL = environment.API_URL;
    this.httpClient = new HttpClient(httpBackend)
  }

  getArticles() {
    return this.http.get<Article[]>(`${this.BASE_URL}/article/all`);
  }

  deleteArticle(id:number){
    return this.httpClient.delete<Article>(`${this.BASE_URL}/article/delete/${id}`).pipe(
      map(() => true)
    );
  }
}
