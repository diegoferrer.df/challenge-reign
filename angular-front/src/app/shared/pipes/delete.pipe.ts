import { Pipe, PipeTransform } from '@angular/core';
import { Article } from '../interfaces/article.interface';

@Pipe({
  name: 'delete'
})
export class DeletePipe implements PipeTransform {

  transform(listArticle: Article[], id:number): any {
    return listArticle.filter(article => article.story_id != id)
  }

}
