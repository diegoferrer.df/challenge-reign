import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { ArticleComponent } from './components/article/article.component';
import { DeletePipe } from './pipes/delete.pipe';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    SpinnerComponent,
    ArticleComponent,
    DeletePipe
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports:[
    SpinnerComponent,
    ArticleComponent,
    DeletePipe,
    ReactiveFormsModule
  ],
})
export class SharedModule { }
