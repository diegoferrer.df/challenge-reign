export interface Article {
  story_id:number;
  story_title:string;
  title:string;
  author:string;
  created_at:string;
  story_url:string,
  url:string
}
