import { Component, Input, OnInit, Output,EventEmitter } from '@angular/core';
import { Article } from '../../interfaces/article.interface';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {

  @Input()
  article:Article = {
    story_id:0,
    story_title: '',
    title: '',
    author: '',
    created_at: '',
    story_url:'',
    url:''
  }

  @Output()
  delete = new EventEmitter;

  constructor() {}

  ngOnInit(): void {
  }

  deleteArticle(id:number){
    this.delete.emit(id)
  }

  goToLink(event:any){
    const url = this.article.story_url || this.article.url
    !event.target.classList.contains('delete') && window.open(url, "_blank");
  }

}
