# Challenge-Reign
## Download repository

```bash
# Clone repository
$ git clone https://gitlab.com/diegoferrer.df/challenge-reign.git

# Open terminal in path repository
  yourPath/yourPath/challenge-reign
```

## Running the app

```bash
# build images docker
  docker-compose build

# up containers
  docker-compose up
```

## View the app

```bash
# Open your navigator (chrome,firefox,etc)
  http://localhost:4200

# Only User y password accepted:
  user: admin
  password: admin
```