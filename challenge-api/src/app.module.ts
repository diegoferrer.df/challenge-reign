import { HttpModule, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ScheduleModule } from '@nestjs/schedule';
import { ArticleModule } from './article/article.module';
import { AlgoliaService } from './services/algolia.service';
import { SharedModule } from './shared/shared.module';
import { LoggedModule } from './logged/logged.module';

@Module({
  imports: [
    ArticleModule,
    HttpModule,
    ScheduleModule.forRoot(),
    MongooseModule.forRoot('mongodb://mongo/articles-challenge',{
    useNewUrlParser:true
  }),
    SharedModule,
    LoggedModule],
  controllers: [AppController],
  providers: [AppService,AlgoliaService],
})
export class AppModule {}
