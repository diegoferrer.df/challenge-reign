import { HttpService, Injectable } from '@nestjs/common';
import { catchError, map, Observable, of } from 'rxjs';
import { Story } from 'src/interfaces/story.interface';
import * as moment from 'moment';
import { Article } from 'src/article/schemas/article.schema';

@Injectable()
export class AlgoliaService {
  constructor(private http: HttpService) {}

  getDataAlgolia(): Observable<Article[]> {
    const URL_API = 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs'; // Utilizar environments
    return this.http.get(URL_API).pipe(
      map((response) => {
        /*
         * filtramos los objetos que tengan un story_title o title
         * formateamos el objeto con las 5 propiedades que necesitamos
         * formateamos la fecha segun requerimientos de wireframes
         */
        return response.data.hits
          // Eliminamos duplicados con mismo story_id
          .reduce((acc:Story[],current:Story)=> {
            if(!acc.some(object => object.story_id == current.story_id)) acc.push(current)
            return acc
          },[])
          // Nos quedamos con las stories que tengan al menos un titulo, una url y un story_id
          .filter(
            (story: Story) =>
              (story.story_title != null || story.title != null) &&
              (story.story_url != null || story.url != null) &&
              story.story_id != null,
          )

          // Nos quedamos con las propiedades necesarias que vamos a utilizar y formatiamos la fecha segun el caso
          .map((story: Story) => {
            return {
              story_id: story.story_id,
              story_title: story.story_title,
              title: story.title,
              author: story.author,
              story_url: story.story_url,
              url: story.url,
              created_at: moment(story.created_at).calendar(null, {
                lastDay: '[Yesterday]',
                sameDay: 'hh:mm a',
                lastWeek: 'MMM DD',
                sameElse: 'MMM DD'
              }),
            };
          });
      }),
      catchError((error) => of(false)),
    );
  }
}
