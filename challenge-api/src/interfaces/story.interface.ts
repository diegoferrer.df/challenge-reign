import { Document } from "mongoose";

export interface Story {
    created_at: string
    title: string,
    url: string,
    author: string,
    points: string | number,
    story_text: string,
    comment_text: string,
    num_comments: string | number,
    story_id: number,
    story_title: string,
    story_url: string,
    parent_id: number,
    created_at_i: number,
    _tags: string[],
    objectID: number,
    _highlightResult: HighlightResult
} 
  
export interface HighlightResult{
  author: {
    value: string,
    matchLevel: string,
    matchedWords: []
  },
  comment_text: {
      value: string,
      matchLevel: string,
      fullyHighlighted: boolean,
      matchedWords: string[]
  },
  story_title: {
      value: string,
      matchLevel: string,
      matchedWords: []
  },
  story_url: {
      value: string,
      matchLevel: string,
      matchedWords: []
  }
}

export interface Article extends Document{
  readonly story_id:number;
  readonly story_title:string; 
  readonly title:string; 
  readonly author:string; 
  readonly created_at:string; 
  readonly story_url:string; 
  readonly url:string;
}

export interface DeleteArticle extends Document{
  readonly story_id:number;
}