import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Cron, CronExpression } from '@nestjs/schedule';
import moment from 'moment';
import { Model } from 'mongoose';
import { Article, DeleteArticle } from 'src/interfaces/story.interface';
import { AlgoliaService } from 'src/services/algolia.service';
import { UtilsService } from 'src/utils/utils/utils.service';
@Injectable()
export class ArticleService {
  constructor(
    @InjectModel('Article') private readonly articleModel: Model<Article>,
    @InjectModel('DeletedArticle') private readonly deleteArticleModel: Model<DeleteArticle>,
    private algoliaService: AlgoliaService,
    private utilsService:UtilsService
  ) {}

  // Cada una hora va a chequear si hay nueva data y la va a insertar en nuestra DB
  @Cron(CronExpression.EVERY_HOUR)
  async getNewData() {
    console.log('CRON EXECUTED');
    const data = await this.algoliaService.getDataAlgolia().toPromise();

    // Insertar nueva data en la DB    
    return await this.articleModel.insertMany(data,{ ordered: false });
  }

  // Exponer todos los articulos 
  async getArticles() {   
    
    const articles = (await this.articleModel.find()).sort((a:any,b:any) => a.created_at - b.created_at)

    const deletedArticles = await this.deleteArticleModel.find()

    // Si la DB esta vacía (como la primera vez) se pide data
    if(!articles.length){
        const newData = await this.getNewData()

        return this.utilsService.formatArticleResponse(newData)
    }

    // Sacamos los articulos que hayamos eliminado alguna vez
    const articlesForResponse = articles.filter(article => {
      if(!deletedArticles.some(deleteArticle => deleteArticle.story_id == article.story_id)) return article       
    })

    return this.utilsService.formatArticleResponse(articlesForResponse)
  }

  // Borrar un articulo
  async deleteArticles(articleID:number): Promise<Article> {
    const article = await this.articleModel.findOneAndDelete({story_id:articleID});

    // Agregamos esta id document de los story id ya eliminados (para que no los sugiera en el futuro)
    await this.deleteArticleModel.create({story_id:articleID})
    
    return article;
  }
}
