import { Module } from '@nestjs/common';
import { ArticleService } from './article.service';
import { ArticleController } from './article.controller';
import { SharedModule } from 'src/shared/shared.module';
import { MongooseModule } from '@nestjs/mongoose';
import { ArticleSchema } from './schemas/article.schema';
import { DeletedArticleSchema } from './schemas/deletedArticles.schema';

@Module({
  imports: [
    SharedModule,
    MongooseModule.forFeature([
      {name:'Article',schema:ArticleSchema},
      {name:'DeletedArticle',schema:DeletedArticleSchema},
    ])
  ],
  providers: [ArticleService],
  controllers: [ArticleController]
})
export class ArticleModule {}
