import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";

@Schema()
export class DeletedArticle{
    @Prop()
    story_id:string; 
}

export const DeletedArticleSchema = SchemaFactory.createForClass(DeletedArticle)
    
