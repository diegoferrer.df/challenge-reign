import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";

@Schema()
export class Article{

    @Prop()
    story_id:number;

    @Prop()
    story_title:string; 

    @Prop()
    title:string; 

    @Prop()
    author:string; 

    @Prop()
    story_url:string; 

    @Prop()
    url:string; 

    @Prop()
    created_at:string; 
}

export const ArticleSchema = SchemaFactory.createForClass(Article)
    
