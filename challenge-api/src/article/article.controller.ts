import { Controller, Delete, Get, HttpStatus, NotFoundException, Param, Res } from '@nestjs/common';
import { ArticleService } from './article.service';
import { DeleteArticleDTO } from './dto/article.dto';

import * as moment from 'moment';

@Controller('article')
export class ArticleController {

  constructor(private articleService: ArticleService) {}

  @Get('/all')
  async getAllArticles(@Res() response) {
    try {
      const data = await this.articleService.getArticles()
      return response.status(HttpStatus.OK).json(data);
    } catch (error) {
      return response.status(HttpStatus.BAD_REQUEST).json(error);
    }
  }

  @Delete('/delete/:story_id')
  async deleteArticle(@Res() response,@Param() param:DeleteArticleDTO) {
    const {story_id} = param  
    const article = await this.articleService.deleteArticles(story_id)

    if(!article) throw new NotFoundException('Article Does not exist')

    return response.status(HttpStatus.OK).json({
      message:'Article Deleted',
      article
    });
    
  }
}
