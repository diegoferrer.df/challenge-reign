import { HttpModule, Module } from '@nestjs/common';
import { AlgoliaService } from 'src/services/algolia.service';
import { UtilsService } from 'src/utils/utils/utils.service';

@Module({
    imports:[
        HttpModule
    ],
    providers: [
        AlgoliaService,
        UtilsService
    ],
    exports:[
        AlgoliaService,
        UtilsService
    ]
})
export class SharedModule {}
