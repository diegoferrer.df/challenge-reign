import { Injectable } from '@nestjs/common';
import { Article } from 'src/article/schemas/article.schema';

@Injectable()
export class UtilsService {

    formatArticleResponse(articlesDB):Article[]{
        return articlesDB.map(article => {
            return {
                created_at:article.created_at,
                author:article.author,
                title:article.title,
                story_title:article.story_title,
                story_id:article.story_id,
                story_url:article.story_url,
                url:article.url
            }
        })
    }
}
